# coding: utf-8
from sys import exit

import pygame
from pygame.locals import *

from pac import PacMan
from labirinty import Labirinty
from ghost import Ghost
import constants


pygame.init()
screen = pygame.display.set_mode((constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT), 0, 32)
pygame.display.set_caption('Hello Word')
clock = pygame.time.Clock()

labirinty = Labirinty(screen=screen)

pacman = PacMan(life=3)

ghost_red = Ghost(color='red')
ghost_purple = Ghost(color='purple')
ghost_orange = Ghost(color='orange')

labirinty.set_pacman(pacman)
# labirinty.set_ghosts(ghost_red)
labirinty.set_ghosts(ghost_red, ghost_purple, ghost_orange)


def close_game_callback():
	for event in pygame.event.get():
		if event.type == QUIT:
			exit()


while True:
	close_game_callback()
	
	pressed_keys = pygame.key.get_pressed()	
	
	# deve vir antes do pacman.draw()
	screen.blit(pygame.Surface(screen.get_size()), (0, 0))

	labirinty.draw(pressed_keys)
	
	pygame.display.update()

	clock.tick(60)
