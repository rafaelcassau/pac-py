# coding: utf-8

import pygame
from pygame.locals import *
import constants


class PacMan(object):

	def __init__(self, life):
		self.life = life
		self.tablet_count = 0
		self.speed = 3
		self.x = 0
		self.y = 0
		self.sprite_x = 40
		self.sprite_y = 40

		self.labirinty = None # seta o labirinto quando a instancia for adicionada ao mesmo

		self.sprites = {
			'pacman-up': pygame.image.load('../sprites/pacman-up.png').convert_alpha(),
			'pacman-down': pygame.image.load('../sprites/pacman-down.png').convert_alpha(),
			'pacman-left': pygame.image.load('../sprites/pacman-left.png').convert_alpha(),
			'pacman-right': pygame.image.load('../sprites/pacman-right.png').convert_alpha(),
		}
		self.current_sprite = self.sprites.get('pacman-right')


	def draw(self, pressed_keys):

		self._dump_screen()
		self._dump_wall()
		self._dump_ghost()
		self._dump_tablet()
		self._eat()
		self._move(pressed_keys)

	def _move(self, pressed_keys):
		if pressed_keys[K_UP]:
			self.current_sprite = self.sprites.get('pacman-up')
			self.y = self.y - self.speed
		elif pressed_keys[K_DOWN]:
			self.current_sprite = self.sprites.get('pacman-down')
			self.y = self.y + self.speed
		elif pressed_keys[K_LEFT]:
			self.current_sprite = self.sprites.get('pacman-left')
			self.x = self.x - self.speed
		elif pressed_keys[K_RIGHT]:
			self.current_sprite = self.sprites.get('pacman-right')
			self.x = self.x + self.speed

		self.labirinty.screen.blit(self.current_sprite, (self.x, self.y))

	def _eat(self):
		pass

	def _dump_screen(self):
		if self.x + self.sprite_x < 0:
			self.x = constants.SCREEN_WIDTH
		
		if self.x > constants.SCREEN_WIDTH:
			self.x = 0 - self.sprite_x

		if self.y > constants.SCREEN_HEIGHT:
			self.y = 0 - self.sprite_y

		if self.y + self.sprite_y < 0:
			self.y = constants.SCREEN_HEIGHT		

	def _dump_wall(self):
		return False

	def _dump_tablet(self):
		return True

	def _dump_ghost(self):
		for ghost in self.labirinty.ghosts:
			if constants.SCREEN_HEIGHT - ghost.y == constants.SCREEN_HEIGHT - self.y:
				if ghost.x < self.x:
					if ghost.x + ghost.sprite_x >= self.x:
						self.labirinty.ghosts.remove(ghost)
				else:
					if self.x + self.sprite_x >= ghost.x:
						self.labirinty.ghosts.remove(ghost)

			elif constants.SCREEN_WIDTH - ghost.x == constants.SCREEN_WIDTH - self.x:
				if ghost.y < self.y:
					if ghost.y + ghost.sprite_y > self.y:
						self.labirinty.ghosts.remove(ghost)
				else:
					if self.y + self.sprite_y > ghost.y:
						self.labirinty.ghosts.remove(ghost)	




