# coding: utf-8


class Labirinty(object):

	def __init__(self, screen):
		self.screen = screen
		self.pacman = None
		self.ghosts = None

	def set_pacman(self, pacman):
		self.pacman = pacman
		self.pacman.labirinty = self

	def set_ghosts(self, *ghosts):
		self.ghosts = list(ghosts)
		for ghost in ghosts:
			ghost.labirinty = self

	def draw(self, pressed_keys):
		self.pacman.draw(pressed_keys)
		for ghost in self.ghosts:
			ghost.draw()			