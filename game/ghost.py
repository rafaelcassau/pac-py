# coding: utf-8
import random
import pygame
import constants


class Ghost(object):

	DIRECTIONS = (
		('X', 1),
		('X', -1),
		('Y', 1),
		('Y', -1),
	)

	def __init__(self, color):
		self.speed = 2
		self.color = color

		self.direction = None
		self._set_new_direction()

		self.sprite_x = 40
		self.sprite_y = 40
		self.x = constants.SCREEN_WIDTH / 2. - self.sprite_x / 2.
		self.y = constants.SCREEN_HEIGHT / 2. - self.sprite_y / 2.
		
		self.labirinty = None # seta o labirinto quando a instancia for adicionada ao mesmo

		ghost_left_key = 'ghost-{}-left'.format(self.color)
		ghost_right_key = 'ghost-{}-left'.format(self.color)

		self.sprites = {
			ghost_left_key: pygame.image.load('../sprites/ghost-{}-left.png'.format(self.color)).convert_alpha(),
			ghost_right_key: pygame.image.load('../sprites/ghost-{}-right.png'.format(self.color)).convert_alpha(),
		}
		self.current_sprite = self.sprites.get(ghost_right_key)

	def draw(self):
		self._move()
		self._dump_wall()

	def _move(self):
		eixo, side = self.direction
		if eixo == 'X':
			self.x += self.speed * side
		else:
			self.y += self.speed * side

		self.labirinty.screen.blit(self.current_sprite, (self.x, self.y))

	def _dump_wall(self):
		if self.x < 0:
			self.x = 0
			self._set_new_direction()

		if self.x + self.sprite_x > constants.SCREEN_WIDTH:
			self.x = constants.SCREEN_WIDTH - self.sprite_x
			self._set_new_direction()
			
		if self.y < 0:
			self.y = 0
			self._set_new_direction()

		if self.y + self.sprite_y > constants.SCREEN_HEIGHT:
			self.y = constants.SCREEN_HEIGHT - self.sprite_y
			self._set_new_direction()
		
	def _set_new_direction(self):
		position = random.choice(range(0, 4))
		self.direction = Ghost.DIRECTIONS[position]
